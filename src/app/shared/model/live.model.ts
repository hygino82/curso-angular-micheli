export class Live {
  id: string;
  liveName: string;
  channelName: string;
  liveDate: string;
  liveLink: string;
  registrationDate: string;
}
